# TDD Php 🐘
@(WebDev)[webdev, php]

### Links
- [What is TDD and its phases?](https://www.whizlabs.com/blog/what-is-tdd-and-its-phases/)


### TDD has the following steps:

1. Think & write test cases
2. Red – Failure of test case
3. Green – Code and get the new test case pass
4. Green – Ensure all old test cases also pass
5. Refactor the code to clean it


![Phases of TDD](https://www.whizlabs.com/wp-content/uploads/2016/04/TDD.png)


#### Run tests
```
vendor\bin\phpunit
```